Project Name
===
   REDAO

Team Name
===
   Land Transcendence:
   - Daniel Saliba
   - Barry Rowe
   - Jean-Baptiste

Short description
===
   A global real estate DAO to enable global stakeholders to participate and create authentic and unique real estate digital twins.

Long description
===
   Trillions of dollars ad billions of people from the real estate system are disconnected the wealth creation happening right now in the web3 an metaverse realms.  The first step to do this is to tie real world real estate to a blockchain identity and build up that identity over time with additional data (gps coordiates, pictures, 3d views, tax payments, real estate transactions, etc.).
   Once a user has timestamped their data to the blockchain and has an identity, they can participate in commutity voting systems, as well as the larger metaverse system as a whole.

Tech Stack
===
  - Web3.js
  - Solidity
  - Remix 
  - Python 
  - Tornado Web
  - MongoDB

Payment Address (USDC on Milkomeda)
===
0xEe3F7346d100FF28648c4Ff2c551FdF58E64c8bb

Project Link
===
   - Live Demo: https://satotest.araa.land/milkomeda/timestamp
   - Repo: https://gitlab.com/spherebeaker/milkomeda-timestamp

Documentation on how to run the project
===
   1. Clone the repo
   2. Run `python setup.py install`, 
   3. Start a MongoDB instance on port 27016
   4. Run `python serve.py` and go to http://localhost:5440/timestamp

   Or just go to the demo website.

Contracts
===
   timestamp.sol ( `/timestamp/contract/timestamp.sol` )

Frontend code (if applicable)
===
   - See `/timestamp/static/`

Backend code:
===
   = See `/timestamp/serve.py`

Screens / graphic materials (optional)
===

Project social media links (if applicable)
===

Recorded pitch ( please send it also to iwo@milkomeda.com)
===

   Pitch: https://ztranslate.net/download/redao.mp4?owner=
