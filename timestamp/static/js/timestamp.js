var server_url = "http://localhost:5440"
async function updateAddress() {
  //update address string
  //update token amount
  var address = ethereum.selectedAddress;
  var balance = await timestamp.balanceOf(address);
  balance = parseInt(balance.toString())
  var elm1 = document.getElementById("address");
  var elm2 = document.getElementById("balance");
  elm1.innerText = address;
  elm2.innerText = balance+" TS"
}

function setMessage(text, isError) {
  var mes = document.getElementById("message");
  mes.innerText = text;
  if (isError) {
    mes.style.color = 'red';
  }
  else {
    mes.style.color = 'green';
  }
}


function setConnectMessage(text, isError) {
  var mes = document.getElementById("message_connect");
  mes.innerText = text;
  if (isError) {
    mes.style.color = 'red';
  }
  else {
    mes.style.color = 'green';
  }
}


async function submitTimestamp() {
  var jurisdiction = document.getElementById("jurisdiction");
  var realestate_id = document.getElementById("id");
  var owner = document.getElementById("owner");
  var files = document.getElementById("files");
  //verify text input
  if (!jurisdiction.value.trim()) {
    return setMessage("Jurisdiction not set", true);
  }
  if (!realestate_id.value.trim()) {
    return setMessage("ID not set", true);
  }
  if (!owner.value.trim()) {
    return setMessage("Owner not set", true);
  }
  if (files.files.length == 0) {
    return setMessage("Files/Hash not set", true);
  }

  var submitData = {
    "jurisdiction": jurisdiction.value.trim(),
    "id": realestate_id.value.trim(),
    "owner": owner.value.trim(),
  };
 
  var raw_data = "Jurisdiction: "+jurisdiction.value.trim()+"\n";
      raw_data+= "ID: "+realestate_id.value.trim()+"\n";
      raw_data+= "Owner: "+owner.value.trim()+"\n";
  var raw_files= "Files: \n";

  //hash the files (or use existing hash)
  var resultList = new Array();
  if (files.files.length > 0) {
    for (var i=0;i<files.files.length;i++) {
      data = await readFileAsync(files.files[i]);
      resultList.push({"data": data, "filename": files.files[i]['name']});
    }
    var hashes = new Array();
    var total_hash = "";
    var encoder = new TextEncoder()
    var buffers = [safe_buffer.Buffer(encoder.encode(jurisdiction)),
                   safe_buffer.Buffer(encoder.encode(realestate_id)),
                   safe_buffer.Buffer(encoder.encode(owner))];

    for (var i=0;i<resultList.length;i++) {
      var buff = _base64ToArrayBuffer(resultList[i]['data']);
      var hash = await sha256_bin(buff);
      resultList[i]['hash'] = hash;
      
      buffers.push(safe_buffer.Buffer(buff));
      raw_files+=resultList[i]['filename']+" hash: "+resultList[i]['hash']+"\n";

    }
    var total_hash = await sha256_bin(safe_buffer.Buffer.concat(buffers));
      
  }
  raw_data += "Total hash: "+total_hash+"\n\n"+raw_files;  
  submitData['hash_key'] = total_hash;
  console.log(submitData);

  await submitDataToContract(submitData);
  console.log(raw_data);
  address = ethereum.selectedAddress;
  if (total_hash.substr(0,2) != "0x")
    total_hash = "0x"+total_hash;
  await IPFSStore(address+'-'+total_hash, raw_data);
}


async function IPFSStore(hash_key, data) {
  //mocking out IPFS for simplicity.
  await sendRequest("POST", "./ipfs?key="+hash_key,data);  
}

async function IPFSRead(hash_key) {
  res =  await sendRequest("GET", "./ipfs?key="+hash_key);  
  console.log(res);
  return res;
}

async function appendTimestamp() {
  var hash = document.getElementById("hash");
  var link = document.getElementById("link");
  
  if (!hash.value.trim()) {
    return setMessage("Files/Hash not set", true);
  }
  if (!link.value.trim()) {
    return setMessage("Link not set", true);
  }


  var submitData = {}
  hash = hash.value.trim()
  if (hash.substr(0,2) == "0x") {
    hash = hash.substr(2, hash.length);
  }
  link = link.value.trim()

  submitData['hash_key'] = hash;
  submitData['link_data'] = link;
  console.log("===========");
  console.log(submitData);
  await submitDataToContract(submitData);
}



async function submitDataToContract(submitData) {
 /*
   {"jurisidiction": ..
    "owner": ...
    "id": ...
    "hash_key": ...
    "link_data": ...
   }
  */
  if (submitData['link_data']) {
    var link_data = submitData['link_data'];
  } else {
    var link_data = submitData['jurisdiction']+"|"+submitData['id']+"|"+submitData['owner'];
  }
  console.log(submitData['hash_key']);
  var hash_key = hexStringToByteArray(submitData['hash_key']);
  
  await timestamp.addTimestamp(hash_key, link_data);
  setMessage("Posted Data")
  await refreshTimestamps();
}

async function refreshTimestamps() {
  var prom = updateAddress();
  ts_data = await getTimestamps();
  var elm = document.getElementById("timestamps")
  var html = '<div class="timestamps_grid">'+
             ' <div class="label">TID</div>'+
             ' <div class="label">Time</div>'+
             //' <div class="label">Jurisdiction</div>'+
             //' <div class="label">ID</div>'+
             //' <div class="label">Owner</div>';
             ' <div class="label">Data (Jurisdiction|ID|Owner)</div>';
  for (var i=0;i<ts_data.length;i++){
    var tt = ts_data[i];
    var hk = escapeHTML(tt['hash_key']);
    if (hk.substr(0,2) != "0x")
      hk = "0x"+hk
    var hk_short = hk.substring(0,8)+"...";
    var address = ethereum.selectedAddress;
    for (var j=0;j<tt['entries'].length;j++) {
      var blockNumber = ts_data[i]['blocks'][j]
      blockNumber = parseInt(blockNumber.toBigInt().toString());
      var time = (await gProvider.getBlock(blockNumber)).timestamp;
      var dd = new Date(time*1000);
      //time = (dd.getMonth()+1)+"-"+dd.getDate()+"-"+dd.getFullYear();
      time = dd.toLocaleDateString()+" "+dd.toLocaleTimeString();
      if (j==0) {
        title_data = await IPFSRead(address+"-"+hk);
        console.log("00000000000000000000000")
        console.log(title_data);
        title_data = hk+"\n\n"+title_data;
        html+='<div class="copy_grid"><div title="'+title_data+'">'+hk_short+'</div>';
        html+='<img src="./static/img/copy.png"/ style="width:10px;cursor:pointer;" title="Copy Hash" onclick="copyToClipboard(\''+hk.trim()+'\');"/></div>';
      }
      else {
        html+="<div></div>";
      }

       
      html+='<div class=""><span class="nobreak">'+time+'</span></div>'+
            '<div>'+escapeHTML(tt['entries'][j])+'</div>'
            //'<div></div>';
            //'<div></div>';
    }
  }
  elm.innerHTML = html;
  await prom;
}

async function getTimestamps() {
  var address = ethereum.selectedAddress; 
  var hashes = await timestamp.getHashKeys(address)//ethers.utils.toUtf8Bytes("00000000000000000000000000000000");
  var results = []
  for (var i=0;i<hashes.length;i++) {
    var data = {
      "hash_key": hashes[i],
      "num": 0,
      "entries": [],
      "blocks": [],
    };
     
    var num = await timestamp.getNumberEntries(ethereum.selectedAddress, hashes[i]);
    data['num'] = num;
    for (var j=0;j<num;j++) {
      ts = await timestamp.getTimestamp(ethereum.selectedAddress, hashes[i],j);
      data['entries'].push(ts)
      block = await timestamp.getTime(ethereum.selectedAddress, hashes[i],j);
      data['blocks'].push(block)
 
    }
    results.push(data)
  }
  console.log(results);
  return results;
}


// Messy ethereum/web3 code now:

const { ethereum } = window;

async function onPageLoad(){
  console.log("99999999999999999999")
  if (Boolean(ethereum && ethereum.isMetaMask)) {
    loadContracts();
    if (!ethereum.isConnected() || !ethereum.selectedAddress) {
      console.log(888888888);
      var elm = document.getElementById("connectMetaMask");
      elm.onclick = clickMetaMask;
      hideDapp();

    } else {
     console.log(1111)
     await showDapp();
    }
  } else {
    setConnectMessage("MetaMask is not installed!", true);
  }
}

async function clickMetaMask() {
 try {
    // Will open the MetaMask UI
    // You should disable this button while the request is pending!
    ethereum.request({ method: 'eth_requestAccounts' }).then(function(x){
      console.log("wallet connected");
      showDapp();
    });
  } catch (error) {
    console.error(error);
  }
}



async function showDapp() {
  var elm1 = document.getElementById("connectMetaMask");
  var elm2 = document.getElementById("dapp");
  elm1.style.display = "none";
  elm2.style.display = "block";
  await refreshTimestamps();
}


function hideDapp() {
  var elm1 = document.getElementById("connectMetaMask");
  var elm2 = document.getElementById("dapp");

  elm1.style.display = "block";
  elm2.style.display = "none";
}


var contract_address = "0x2927d6e6e5E52217397377dC26bD1560EA572A15";
var timestamp;

function loadContracts() {
  // A Web3Provider wraps a standard Web3 provider, which is
  // what Metamask injects as window.ethereum into each page
  gProvider = new _ethers.providers.Web3Provider(window.ethereum)

  // The Metamask plugin also allows signing transactions to
  // send ether and pay to change state within the blockchain.
  // For this, you need the account signer...
  gSigner = gProvider.getSigner()

  
  var timestamp_contract = new _ethers.Contract(
                                       contract_address,
                                       CONTRACT_ABI,
                                       gProvider);
  timestamp = timestamp_contract.connect(gSigner);
}







// Helper functions....

function _base64ToArrayBuffer(base64) {
  var base64_string = base64.substr(base64.indexOf("base64,")+7, base64.length);
  var binary_string = window.atob(base64_string);
  var len = binary_string.length;
  var bytes = new Uint8Array(len);
  for (var i = 0; i < len; i++) {
    bytes[i] = binary_string.charCodeAt(i);
  }
  return bytes.buffer;
}


function readFileAsync(file) {
  return new Promise((resolve, reject) => {
    let reader = new FileReader();

    reader.onload = () => {
      resolve(reader.result);
    };

    reader.onerror = reject;

    reader.readAsDataURL(file);
  })
}

async function sha256(message) {
  // encode as UTF-8
  const msgBuffer = new TextEncoder().encode(message);                    
  return sha256_bin(msgBuffer);
}

async function sha256_bin(msgBuffer) {
  // hash the message
  const hashBuffer = await crypto.subtle.digest('SHA-256', msgBuffer);

  // convert ArrayBuffer to Array
  const hashArray = Array.from(new Uint8Array(hashBuffer));

  // convert bytes to hex string                  
  const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join('');
  return hashHex;
}

function isHex(h) {
var a = parseInt(h,16);
return (a.toString(16) === h)
}


function hexStringToByteArray(hexString) {
    if (hexString.length % 2 !== 0) {
        throw "Must have an even number of hex digits to convert to bytes";
    }/* w w w.  jav  a2 s .  c o  m*/
    var numBytes = hexString.length / 2;
    var byteArray = new Uint8Array(numBytes);
    for (var i=0; i<numBytes; i++) {
        byteArray[i] = parseInt(hexString.substr(i*2, 2), 16);
    }
    return byteArray;
}

const escapeHTML = (unsafe) => {
    return unsafe.replaceAll('&', '&amp;').replaceAll('<', '&lt;').replaceAll('>', '&gt;').replaceAll('"', '&quot;').replaceAll("'", '&#039;');
}



function copyToClipboard (str) {
  const el = document.createElement('textarea');
  el.value = str;
  el.setAttribute('readonly', '');
  el.style.position = 'absolute';
  el.style.left = '-9999px';
  document.body.appendChild(el);
  el.select();
  document.execCommand('copy');
  document.body.removeChild(el);
};

function hexToBytes(hex) {
    for (var bytes = [], c = 0; c < hex.length; c += 2)
        bytes.push(parseInt(hex.substr(c, 2), 16));
    return bytes;
}

function sendRequest(method, uri, body) {
  return new Promise(function(resolve, reject) {
    var http = new XMLHttpRequest();
    var url=uri;
    http.open(method, url);
    if (body)
      http.send(body);
    else
      http.send();

    http.onreadystatechange = function(e) {
      if (this.readyState == 4 && this.status == 200)
          resolve(http.responseText);
    }
  });
}
