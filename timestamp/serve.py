import tornado.web
import tornado.ioloop
import pymongo

conn = pymongo.MongoClient("localhost", 27017)
db = conn.milkomeda

class TimestampHandler(tornado.web.RequestHandler):
    def get(self):
        html = open("./static/html/timestamp.html").read()
        self.write(html)




class IPFSMockHandler(tornado.web.RequestHandler):
    def get(self):
        key = self.get_argument("key")
        doc = db.timestamp.find_one({"_id": key})
        if doc:
            self.write(doc['data'])
        else:
            self.write({"error": "not found"})

    def post(self):
        key = self.get_argument("key")
        db.timestamp.update({"_id": key},
                            {"$set": {"data": self.request.body}}, 
                            upsert=True)        


def main():
    app = tornado.web.Application([
      (r'/milkomeda/timestamp', TimestampHandler),
      (r"/milkomeda/ipfs", IPFSMockHandler),
      (r'/milkomeda/static/(.*)', tornado.web.StaticFileHandler,
          {'path': "./static"})
    ])
    app.listen(5440)
    tornado.ioloop.IOLoop.current().start()


if __name__=="__main__":
    main()

