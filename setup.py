from setuptools import setup, find_packages

__VERSION__ = "1.0.0"

def main(args=None):
    README = open("./README").read()

    setup_required_packages = []

    required_packages = ["pymongo<4.0", "tornado==5.0.2", 
                         "crypto==1.4.1", "pycrypto==2.6.1",
                         "numpy==1.16.3", "pillow", "ecdsa",
                         "base58", "six", "python-dateutil",
                         "qrcode"
                        ]

    test_required_packages = ["nose", "coverage"]

    settings = dict(name="lt_timestamp",
                    version=__VERSION__,
                    description="lt_timestamp",
                    long_description=README,
                    classifiers=["Programming Language :: Python", ],
                    author="",
                    author_email="",
                    url="",
                    keywords="timestamp",
                    packages=find_packages(),
                    include_package_data=True,
                    zip_safe=False,
                    install_requires=required_packages,
                    tests_require=test_required_packages,
                    test_suite="nose.collector",
                    setup_requires=setup_required_packages,
                    entry_points="""\
                        [console_scripts] 
                        lt_serve=lt_timestamp.serve:main
                        """,
                    )
    if args:
        settings['script_name'] = __file__
        settings['script_args'] = args
    setup(**settings)


if __name__ == "__main__":
    main()
